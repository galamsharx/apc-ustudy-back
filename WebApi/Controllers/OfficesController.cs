﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using UPlanner.Application.Auth.Services;
using UPlanner.Application.Planning.UseCases;

namespace UPlanner.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OfficesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ISecurityContext _securityContext;

        public OfficesController(IMediator mediator, ISecurityContext securityContext)
        {
            _mediator = mediator;
            _securityContext = securityContext;
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> AddOffice([FromBody] AddOfficeUseCase useCase)
        {
            await _mediator.Send(useCase);
            return NoContent();
        }

        [HttpPost("add/exam")]
        [Authorize]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> AddExamToOffice([FromBody] AddExamToOfficeUseCase useCase)
        {
            await _mediator.Send(useCase);
            return NoContent();
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetOfficesUseCaseOutput))]
        public async Task<IActionResult> GetOffices([FromQuery] GetOfficesUseCase useCase)
        {
            return Ok(await _mediator.Send(useCase));
        }

        [HttpGet("region")]
        [Authorize]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetOfficesUseCaseOutput))]
        public async Task<IActionResult> GetOfficesByRegion([FromQuery] GetOfficesUseCase useCase)
        {
            var userId = _securityContext.UserId;
            var user = await _mediator.Send(new GetUserUseCase(userId));
            useCase.RegionId = user.User.RegionId;
            return Ok(await _mediator.Send(useCase));
        }
    }
}
