﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using UPlanner.Application.Common.Models;
using UPlanner.Application.Planning.UseCases;

namespace UPlanner.WebApi.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RegionsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RegionsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> AddRegion([FromBody] AddRegionUseCase useCase)
        {
            await _mediator.Send(useCase);

            return NoContent();
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetRegionsUseCaseOutput))]
        public async Task<IActionResult> GetRegions([FromQuery] GetRegionsUseCase useCase)
        {
            return Ok(await _mediator.Send(useCase));
        }
    }
}
