﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using UPlanner.Application.Auth.UseCases;

namespace UPlanner.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AuthenticationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("token")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetTokenUseCaseOutput))]
        public async Task<IActionResult> Token([FromBody] GetTokenUseCase useCase)
        {
            return Ok(await _mediator.Send(useCase));
        }
    }
}
