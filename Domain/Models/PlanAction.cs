﻿using System;

namespace UPlanner.Domain.Models
{
    public class PlanAction
    {
        public Guid Id { get; set; }

        public int People { get; set; }

        public ActionType Type { get; set; }

        public Guid ProposalId { get; set; }
        public PlanProposal Proposal { get; set; }
        public Guid ExamId { get; set; }
        public Exam Exam { get; set; }

        public DateTimeOffset Date { get; set; }
    }

    public enum ActionType
    {
        AddGroup,
        RemoveGroup
    }
}
