﻿using System;
using System.Collections.Generic;

namespace UPlanner.Domain.Models
{
    public class User
    {
        public Guid Id { get; set; }

        public string Login { get; set; }
        public string PasswordHash { get; set; }

        public Role Role { get; set; }

        public Guid RegionId { get; set; }
        public Region Region { get; set; }
        public List<PlanProposal> Proposals { get; set; }
    }
}
