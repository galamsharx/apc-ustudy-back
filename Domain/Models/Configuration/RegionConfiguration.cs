﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPlanner.Domain.Models.Configuration
{
    public class RegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.HasData(
                    new() { Id = Guid.Parse("00000000-0000-0000-0000-000000000001"), Name = "General" },
                    new() { Id = new Guid("5fe90a81-e723-43b7-9613-2fdcb9f4585e"), Name = "Алматинская" },
                    new() { Id = new Guid("ea70f091-1d51-4477-9d4f-8879103bb8f7"), Name = "Актюбинская" },
                    new() { Id = new Guid("017567f4-e687-446d-a2d7-0f2df5798200"), Name = "Акмолинская" },
                    new() { Id = new Guid("9b7648ca-3989-4ddd-9356-00c20324f6b8"), Name = "Атырауская" },
                    new() { Id = new Guid("b99e5dab-18e7-496d-8d00-aab6ebe1754c"), Name = "ВКО" },
                    new() { Id = new Guid("95069bca-bf8c-4371-b070-b44c288c248f"), Name = "Жамбылская" },
                    new() { Id = new Guid("ed62791f-83c5-4219-9dec-19f21be1605f"), Name = "ЗКО" },
                    new() { Id = new Guid("c1e223eb-8ee9-4bff-a6ae-1dade6d20039"), Name = "Карагандинская" },
                    new() { Id = new Guid("a9091b3e-61d0-4166-8450-f2c87accbf93"), Name = "Костанайская" },
                    new() { Id = new Guid("97b90ff3-935e-4cd9-9ca4-57504fa12255"), Name = "Кызылординская" },
                    new() { Id = new Guid("cb267347-010b-46c5-a2e9-6eb02c8d3ca0"), Name = "Мангыстауская" },
                    new() { Id = new Guid("a0523143-342a-4be9-81cc-bbe5dd273b41"), Name = "Павлодарская" },
                    new() { Id = new Guid("6b027d06-11f4-4799-a834-694a8cb8885a"), Name = "СКО" },
                    new() { Id = new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173"), Name = "Туркестанская" },
                    new() { Id = new Guid("b97a2348-a405-458b-9700-ee7c3662bb00"), Name = "Алматы" },
                    new() { Id = new Guid("24b24037-afe3-4625-97b9-42e3ff85006b"), Name = "Нур-Султан" },
                    new() { Id = new Guid("b303ac57-8506-4a4d-83c4-f649e2170e3e"), Name = "Шымкент" }
                );
        }
    }
}
