﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace UPlanner.Domain.Models.Configuration
{
    public class OfficeConfiguration : IEntityTypeConfiguration<Office>
    {
        public void Configure(EntityTypeBuilder<Office> builder)
        {
            builder.HasData(
                    new Office() { Id = new Guid("35b7598d-28f1-4d12-aaa1-95aca831950c"), Name = "Талдыкорган", Address = "Северно - западный жилой район ул.Қабанбай батыра 180(25 школа)", RegionId = Guid.Parse("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    new Office() { Id = new Guid("05b65aed-3b4b-4e1b-b56f-467d5ac87f5b"), Name = "Алматинская обл., Ақсуский район, с. Ильяс Жансугуров", Address = "Ораз Жумашев 29", RegionId = Guid.Parse("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    new Office() { Id = new Guid("4cd9c01f-2f56-497c-992e-e3b4f64323dd"), Name = "Алм. Область Карасайский район Шамалган", Address = "ул.Наурыз 62 село Шамалган школа Ушконыр", RegionId = Guid.Parse("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    new Office() { Id = new Guid("a59b361a-21d7-44b1-9056-29ec34b95e0b"), Name = "Алм. Область Енбекшиказахский  район Есик", Address = "город Есик ул Райымбека 44 школа Райымбек батыра", RegionId = Guid.Parse("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    new Office() { Id = new Guid("c564c92c-8780-495c-ad0d-63d93ee5d9b4"), Name = "Алм. Область Панфиловский  район Жаркент", Address = "улица Кастеева 12", RegionId = Guid.Parse("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    new Office() { Id = new Guid("430127c8-68c1-4573-b50a-e67d42b652f2"), Name = "Актобе", Address = "Алматинский р-н, ул.Арынова 1", RegionId = Guid.Parse("ea70f091-1d51-4477-9d4f-8879103bb8f7") },
                    new Office() { Id = new Guid("887f862e-8c48-4f56-a455-de4739f0b8a2"), Name = "п.Қарауылкелді ", Address = "ул. Д.Қонаев  24", RegionId = Guid.Parse("ea70f091-1d51-4477-9d4f-8879103bb8f7") },
                    new Office() { Id = new Guid("b878ec9f-6221-4b39-bb97-60cb68c8b62d"), Name = "г.Шалкар ", Address = "ул.Ә.Жангелдин 89А", RegionId = Guid.Parse("ea70f091-1d51-4477-9d4f-8879103bb8f7") },
                    new Office() { Id = new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2"), Name = "Кокшетау", Address = "ул. Акана-серэ, 24", RegionId = Guid.Parse("017567f4-e687-446d-a2d7-0f2df5798200") },
                    new Office() { Id = new Guid("5abc87e4-e038-4b79-8ecf-bce61c2ecffb"), Name = "Атбасарский район, Атбасар ", Address = "Жеңіс 86", RegionId = Guid.Parse("017567f4-e687-446d-a2d7-0f2df5798200") },
                    new Office() { Id = new Guid("a3bc3719-694c-427f-a605-6ba6b498ab4a"), Name = "Акколский район. Акколь", Address = "Нурмагамбетова 144", RegionId = Guid.Parse("017567f4-e687-446d-a2d7-0f2df5798200") },
                    new Office() { Id = new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e"), Name = "Атырау", Address = "ул. Баймуханова, 45а, АИНГ", RegionId = Guid.Parse("9b7648ca-3989-4ddd-9356-00c20324f6b8") },
                    new Office() { Id = new Guid("21b7c4c4-a328-4fbd-b253-35229b08c1c4"), Name = "г. Кульсары", Address = "улица 199, дом 1", RegionId = Guid.Parse("9b7648ca-3989-4ddd-9356-00c20324f6b8") },
                    new Office() { Id = new Guid("32ed3077-8c02-41c3-bc85-bcebcb0cca20"), Name = "Ганюшкино", Address = "ул. Отан дом 17 (Курмангазинский агро-технический колледж)", RegionId = Guid.Parse("9b7648ca-3989-4ddd-9356-00c20324f6b8") },
                    new Office() { Id = new Guid("4a251f8c-f0cf-4813-9205-068fffe744a5"), Name = "Усть-Каменогорск", Address = "Н.Назарбаева 86/2 (КАСУ)", RegionId = Guid.Parse("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    new Office() { Id = new Guid("0b4d6ca7-9eb2-4c61-a4e0-2eb68a9c0d7e"), Name = "Семей", Address = "ул. Танирбергенова, 1", RegionId = Guid.Parse("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    new Office() { Id = new Guid("6ef5ac8e-275b-488e-bcb4-ca9c067e59a4"), Name = "с.Улкен Нарын", Address = "ул. Шулятикова 64", RegionId = Guid.Parse("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    new Office() { Id = new Guid("df370a80-c331-4b21-8cb2-22ed9d41ddac"), Name = "с. Аксуат ", Address = "ул.Муканова 20", RegionId = Guid.Parse("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    new Office() { Id = new Guid("253a277c-40a1-4bbb-b051-f275b3540b9f"), Name = "г.Аягоз ", Address = "ул.Кабанбай Батыра, строение 5/1", RegionId = Guid.Parse("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    new Office() { Id = new Guid("8781c7d6-f221-41f0-a150-300f6504bc5a"), Name = "Тараз", Address = "Тауке хана 24", RegionId = Guid.Parse("95069bca-bf8c-4371-b070-b44c288c248f") },
                    new Office() { Id = new Guid("a6db74eb-a0b1-405a-b395-c9871ec75bc3"), Name = "Шуский район село Толеби ", Address = "Шуский район село Толеби ул.Кунаева 92 ", RegionId = Guid.Parse("95069bca-bf8c-4371-b070-b44c288c248f") },
                    new Office() { Id = new Guid("9ab64280-b541-4b0a-9c90-15d7c7ecaaf2"), Name = "Уральск", Address = "ТЦ \"Форум\", 4 этаж, ул. Ж. Молдагалиева 18", RegionId = Guid.Parse("ed62791f-83c5-4219-9dec-19f21be1605f") },
                    new Office() { Id = new Guid("5d9f00a4-c826-4403-92d8-15bf0291c237"), Name = "г. Аксай", Address = "ул. Молодежная 29", RegionId = Guid.Parse("ed62791f-83c5-4219-9dec-19f21be1605f") },
                    new Office() { Id = new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca"), Name = "Караганда", Address = "Ул.Казахстанская 15А", RegionId = Guid.Parse("c1e223eb-8ee9-4bff-a6ae-1dade6d20039") },
                    new Office() { Id = new Guid("ae8750ad-41a5-48b5-952a-66913f648382"), Name = "г.Балхаш", Address = "ул.Караменде би, 17", RegionId = Guid.Parse("c1e223eb-8ee9-4bff-a6ae-1dade6d20039") },
                    new Office() { Id = new Guid("df1cfdc6-0f30-4d31-9c68-57997d7cb90f"), Name = "г.Жезказган", Address = "ул.Байконурова 123", RegionId = Guid.Parse("c1e223eb-8ee9-4bff-a6ae-1dade6d20039") },
                    new Office() { Id = new Guid("c34ccf6a-d27e-4a17-9868-99b855abed0b"), Name = "Костанай", Address = "ул.Тәеулсіздік 118, французский культурный центр,2 этаж", RegionId = Guid.Parse("a9091b3e-61d0-4166-8450-f2c87accbf93") },
                    new Office() { Id = new Guid("e6323325-bd98-4306-b4c3-ca8cea161ca2"), Name = "г.Лисаковск", Address = "6 микрорайон, дом 56", RegionId = Guid.Parse("a9091b3e-61d0-4166-8450-f2c87accbf93") },
                    new Office() { Id = new Guid("a09c37a8-74f4-4a82-8282-6fc1ba2cb4ef"), Name = " г.Аркалык    ", Address = "ул.Мауленова 26 (Аркалыкский многопрофильный колледж Казпотребсоюза)", RegionId = Guid.Parse("a9091b3e-61d0-4166-8450-f2c87accbf93") },
                    new Office() { Id = new Guid("88b4b9bd-82f2-49ba-b168-850809a54cbe"), Name = "Кызылорда", Address = "ул Тажибаева,18а", RegionId = Guid.Parse("97b90ff3-935e-4cd9-9ca4-57504fa12255") },
                    new Office()
                    {
                        Id = new Guid("16b937e8-91b7-4b4b-9a77-e55e615d9c54"),
                        Name = "Кармакшинский район, п.Жосалы",
                        Address = "Балқы Базар 19 А,Тәйімбет Көмекбаев атындағы №250 мектеп - лицей",
                        RegionId = Guid.Parse("97b90ff3-935e-4cd9-9ca4-57504fa12255")
                    },
                    new Office()
                    {
                        Id = new Guid("5b0e85d0-f832-4d5e-8887-8d98ee5190de"),
                        Name = "п.Жанакорган",
                        Address = " Үрзімат Мадиев 51,№110 мектеп - лицей",
                        RegionId = Guid.Parse("97b90ff3-935e-4cd9-9ca4-57504fa12255")
                    },
                    new Office()
                    {
                        Id = new Guid("1d184008-f44c-46a4-99ec-762ba4ac5ee1"),
                        Name = "Казалинский р-н, Кент Айтебе би",
                        Address = "Ж.Нұрмағанбетұлы көшесі 128,Е.Бозғұлов атындағы №249 мектеп - лицей",
                        RegionId = Guid.Parse("97b90ff3-935e-4cd9-9ca4-57504fa12255")
                    },
                    new Office() { Id = new Guid("ee8c293b-23f0-4f0c-8d96-fc8dc67c6e3c"), Name = "Актау", Address = "15-й микрорайон, 31    (IT  мектеп-лицей вход с торца, левое крыло)", RegionId = Guid.Parse("cb267347-010b-46c5-a2e9-6eb02c8d3ca0") },
                    new Office() { Id = new Guid("0a667344-4d06-497b-b224-d31dac9cb5e7"), Name = "Бейнеуский р-н, г.Бейнеу", Address = "Школу им Ы.Алтынсарина по адресу Косай ата 15", RegionId = Guid.Parse("cb267347-010b-46c5-a2e9-6eb02c8d3ca0") },
                    new Office()
                    {
                        Id = new Guid("9e8b87b9-0eef-4ae5-8a00-69b6fe84990c"),
                        Name = "г.Жанаозен",
                        Address = "​Отырар, 65а Арай м - н",
                        RegionId = Guid.Parse("cb267347-010b-46c5-a2e9-6eb02c8d3ca0")
                    },
                    new Office() { Id = new Guid("b5ce5731-2299-4090-9b61-df82017a24f5"), Name = "Павлодар", Address = "ул.Толстого 99", RegionId = Guid.Parse("a0523143-342a-4be9-81cc-bbe5dd273b41") },
                    new Office() { Id = new Guid("1a0e2a97-a8a2-45ee-accf-2a04714cf3a2"), Name = "г.Экибастуз", Address = "ул. Энергетиков 54А", RegionId = Guid.Parse("a0523143-342a-4be9-81cc-bbe5dd273b41") },
                    new Office() { Id = new Guid("bbb4d447-5df7-4622-9194-06d900ce7934"), Name = "г. Аксу", Address = "ул. Ауэзова, 6", RegionId = Guid.Parse("a0523143-342a-4be9-81cc-bbe5dd273b41") },
                    new Office() { Id = new Guid("ed58427d-f5b3-46e2-b83d-d9bc83d3ef75"), Name = "Петропавловск", Address = " ул. Конституции Казахстана, 60", RegionId = Guid.Parse("6b027d06-11f4-4799-a834-694a8cb8885a") },
                    new Office() { Id = new Guid("27936879-a81b-4970-9633-11d41777db88"), Name = "р-н Габита Мусрепова г. Новоишимское", Address = "ул. Ауельбекова 2А", RegionId = Guid.Parse("6b027d06-11f4-4799-a834-694a8cb8885a") },
                    new Office() { Id = new Guid("36a26cbd-5ae0-4356-936f-f6cdb030ea1f"), Name = "г.Тайынша", Address = " ул Конституция Казахстана 261.(Тайыншинский колледж агробизнеса)", RegionId = Guid.Parse("6b027d06-11f4-4799-a834-694a8cb8885a") },
                    new Office() { Id = new Guid("2f625dc6-592c-4f76-a771-59d01669e3da"), Name = "Туркестан", Address = "1 мкр 24В (педколледж)", RegionId = Guid.Parse("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    new Office() { Id = new Guid("80ec8904-8b03-4a15-9684-8a8bf48a28af"), Name = "Ордабасы ", Address = "райцентр Темирлан, ул. Қажымұқан 77/2 (жаңа мектеп)", RegionId = Guid.Parse("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    new Office() { Id = new Guid("4b8c3b76-a91f-479f-9ef3-f25ca981e218"), Name = "Сайрам, Ақсу кенті", Address = "Әйтеке би көшесі. 3А «Оңтүстік Қазақстан индустриалды-инновациялық колледжі»", RegionId = Guid.Parse("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    new Office() { Id = new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779"), Name = "Сарыағаш", Address = "Шәмші гүлзары 21а (Қапланбек жоғары аграрлық-техникалық колледжі) ", RegionId = Guid.Parse("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    new Office() { Id = new Guid("d3c368bc-cc3c-48ec-a227-dce8a1a65913"), Name = "Шолаққорған", Address = "Тоқпанбетов 1 а (Нәзір Төрекулов атындағы  IT мектеп лицейі)", RegionId = Guid.Parse("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    new Office() { Id = new Guid("04097107-c061-4d50-874d-f81d0c25db80"), Name = "Жетисай", Address = "КГУ СШ №8 им.Алпамыс батыра. ул.М.Озтурик №13  ", RegionId = Guid.Parse("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    new Office() { Id = new Guid("ea5ab2e0-f21e-483e-9181-abe8ffe23b77"), Name = "Алматы", Address = "Проспект Аль-Фараби, 71", RegionId = Guid.Parse("b97a2348-a405-458b-9700-ee7c3662bb00") },
                    new Office() { Id = new Guid("8e3ef246-7706-4a5f-afa8-b2feb935f64d"), Name = "Алматы 2", Address = "Микрорайон Коктем-1, 26а", RegionId = Guid.Parse("b97a2348-a405-458b-9700-ee7c3662bb00") },
                    new Office() { Id = new Guid("b238d135-dd85-47fa-9764-a2e9172f5f97"), Name = "Есильский р-н", Address = "Тауелсиздик 52", RegionId = Guid.Parse("24b24037-afe3-4625-97b9-42e3ff85006b") },
                    new Office() { Id = new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25"), Name = "Шымкент", Address = "ул.Терешкова 14 А", RegionId = Guid.Parse("b303ac57-8506-4a4d-83c4-f649e2170e3e") },
                    new Office() { Id = new Guid("f3311eee-8103-4312-8235-8f0030a3ed51"), Name = "Шымкент 2", Address = "улица Жандосова, 40/1", RegionId = Guid.Parse("b303ac57-8506-4a4d-83c4-f649e2170e3e") },
                    new Office() { Id = new Guid("7361c396-f717-4bc1-aebd-3ff78609be78"), Name = "Шымкент 3", Address = "ул. Мухамед Хайдар Дулати 225 (3-4 этаж)", RegionId = Guid.Parse("b303ac57-8506-4a4d-83c4-f649e2170e3e") }
                );
        }
    }
}
