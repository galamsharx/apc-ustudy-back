﻿using System.Collections.Generic;

namespace UPlanner.Application.Common.Models
{
    public class PageResponse<T>
    {
        public PageResponse(int total, List<T> items)
        {
            Total = total;
            Items = items;
        }

        public int Total { get; }
        public List<T> Items { get; }
    }
}
