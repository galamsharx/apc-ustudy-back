﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Common.Models;

namespace UPlanner.Application.Common.Extensions
{
    public static class PaginationExtensions
    {
        public static async Task<PageResponse<T>> ToPageAsync<T>(
            this IQueryable<T> query,
            PageRequest pageRequest,
            CancellationToken cancellationToken = default)
        {
            int total = await query.CountAsync(cancellationToken);

            var items = await query
                .Skip(pageRequest.PageIndex * pageRequest.PageSize)
                .Take(pageRequest.PageSize)
                .ToListAsync(cancellationToken);

            return new PageResponse<T>(total, items);
        }
    }
}
