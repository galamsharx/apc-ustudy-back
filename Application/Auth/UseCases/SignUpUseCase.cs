﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Auth.Services;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Auth.UseCases
{
    public class SignUpUseCase : IRequest<SignUpUseCaseOutput>
    {
        public string Login { get; set; }
        public Role Role { get; set; }
        public Guid RegionId {  get; set; }
    }

    public class SignUpUseCaseHandler : IRequestHandler<SignUpUseCase, SignUpUseCaseOutput>
    {
        private readonly PlannerContext _db;
        private readonly IPasswordGenerator _passwordGenerator;
        private readonly IPasswordHasher _passwordHasher;

        public SignUpUseCaseHandler(
            PlannerContext db,
            IPasswordGenerator passwordGenerator,
            IPasswordHasher passwordHasher)
        {
            _db = db;
            _passwordGenerator = passwordGenerator;
            _passwordHasher = passwordHasher;
        }

        public async Task<SignUpUseCaseOutput> Handle(SignUpUseCase request, CancellationToken cancellationToken)
        {
            var password = _passwordGenerator.GeneratePassword();

            var user = new User
            {
                Id = Guid.NewGuid(),
                Login = request.Login,
                Role = request.Role,
                RegionId = request.RegionId,
                PasswordHash = _passwordHasher.Hash(password)
            };

            _db.Users.Add(user);

            await _db.SaveChangesAsync(cancellationToken);

            return new()
            {
                Login = request.Login,
                Password = password
            };
        }
    }

    public class SignUpUseCaseOutput
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
