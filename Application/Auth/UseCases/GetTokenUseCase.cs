﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Auth.Services;
using UPlanner.Application.Planning.Exceptions;
using UPlanner.Domain.Data;

namespace UPlanner.Application.Auth.UseCases
{
    public class GetTokenUseCase : IRequest<GetTokenUseCaseOutput>
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class GetTokenUseCaseHandler : IRequestHandler<GetTokenUseCase, GetTokenUseCaseOutput>
    {
        private readonly PlannerContext _db;
        private readonly IPasswordHasher _passwordHasher;
        private readonly ITokenFactory _tokenFactory;

        public GetTokenUseCaseHandler(
            PlannerContext db,
            IPasswordHasher passwordHasher,
            ITokenFactory tokenFactory)
        {
            _db = db;
            _passwordHasher = passwordHasher;
            _tokenFactory = tokenFactory;
        }

        public async Task<GetTokenUseCaseOutput> Handle(GetTokenUseCase request, CancellationToken cancellationToken)
        {
            var user = await _db.Users.FirstOrDefaultAsync(u => u.Login == request.Login, cancellationToken);

            if (user is null || !_passwordHasher.Verify(request.Password, user.PasswordHash))
            {
                throw new ResponseException() { Status = (int)HttpStatusCode.BadRequest, Value = "Not found" };
            }

            return new GetTokenUseCaseOutput()
            {
                AccessToken = _tokenFactory.CreateToken(user)
            };
        }
    }

    public class GetTokenUseCaseOutput
    {
        public string AccessToken { get; set; }
    }
}
