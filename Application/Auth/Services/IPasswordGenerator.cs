﻿namespace UPlanner.Application.Auth.Services
{
    public interface IPasswordGenerator
    {
        string GeneratePassword();
    }
}
