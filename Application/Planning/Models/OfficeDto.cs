﻿using System;
using System.Collections.Generic;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.Models
{
    public class OfficeDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }

        public Guid RegionId { get; set; }
        public ICollection<Exam> Exams {  get; set; }
    }
}
