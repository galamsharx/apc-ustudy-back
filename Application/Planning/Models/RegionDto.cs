﻿using System;

namespace UPlanner.Application.Planning.Models
{
    public class RegionDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
