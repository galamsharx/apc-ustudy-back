﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPlanner.Application.Planning.Models
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string Login { get; set; }
        public string Role { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
    }
}
