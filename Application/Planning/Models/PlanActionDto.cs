﻿using System;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.Models
{
    public class PlanActionDto
    {
        public ActionType Type { get; set; }
        public int People { get; set; }
        public Guid ExamId { get; set; }
        public string ExamName { get; set; }
        public DateTimeOffset Date { get; set; }
        public bool IsMixable { get; set; }
    }
}
