﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPlanner.Application.Planning.Models
{
    public class ClassroomDto
    {
        public Guid Id { get; set; }
        public Guid OfficeId {  get; set; }
        public int Capacity { get; set; }
        public int TakenPlaces { get; set; }
    }
}
