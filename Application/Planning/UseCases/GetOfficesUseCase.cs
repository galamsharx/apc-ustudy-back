﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Common.Extensions;
using UPlanner.Application.Common.Models;
using UPlanner.Application.Planning.Models;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class GetOfficesUseCase : IRequest<GetOfficesUseCaseOutput>
    {
        public PageRequest PageRequest { get; set; }
        public Guid? RegionId { get; set; }
    }

    public class GetOfficesUseCaseHandler : IRequestHandler<GetOfficesUseCase, GetOfficesUseCaseOutput>
    {
        private readonly PlannerContext _db;

        public GetOfficesUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        public async Task<GetOfficesUseCaseOutput> Handle(GetOfficesUseCase request, CancellationToken cancellationToken)
        {
            IQueryable<Office> query = _db.Offices;

            if (request.RegionId.HasValue && request.RegionId != Guid.Parse("00000000-0000-0000-0000-000000000001"))
            {
                query = query.Where(o => o.RegionId == request.RegionId);
            }

            var page = await query
                .Select(o => new OfficeDto
                {
                    Id = o.Id,
                    Name = o.Name,
                    Address = o.Address,
                    RegionId = o.RegionId,
                    Exams = o.Exams
                })
                .ToPageAsync(request.PageRequest ?? PageRequest.Default(), cancellationToken);

            return new() { Page = page };
        }
    }

    public class GetOfficesUseCaseOutput
    {
        public PageResponse<OfficeDto> Page { get; set; }
    }
}
