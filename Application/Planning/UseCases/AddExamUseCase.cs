﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class AddExamUseCase : IRequest
    {
        public string Name { get; set; }
        public bool Mixable { get; set; }
    }

    public class AddExamUseCaseHandler : AsyncRequestHandler<AddExamUseCase>
    {
        private readonly PlannerContext _db;

        public AddExamUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(AddExamUseCase request, CancellationToken cancellationToken)
        {
            var exam = new Exam
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Mixable = request.Mixable
            };

            _db.Exams.Add(exam);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
