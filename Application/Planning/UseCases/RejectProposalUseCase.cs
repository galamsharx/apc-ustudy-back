﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class RejectProposalUseCase : IRequest
    {
        public Guid ProposalId { get; set; }
    }

    public class RejectProposalUseCaseHandler : AsyncRequestHandler<RejectProposalUseCase>
    {
        private readonly PlannerContext _db;

        public RejectProposalUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(RejectProposalUseCase request, CancellationToken cancellationToken)
        {
            var proposal = await _db.Proposals
                .FindAsync(keyValues: new object[] { request.ProposalId }, cancellationToken: cancellationToken);

            proposal.Status = PlanProposalStatus.Rejected;

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
