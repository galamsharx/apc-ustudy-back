﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class AddClassroomUseCase : IRequest
    {
        public Guid OfficeId { get; set; }
        public int Capacity { get; set; }
    }

    public class AddClassroomUseCaseHandler : AsyncRequestHandler<AddClassroomUseCase>
    {
        private readonly PlannerContext _db;

        public AddClassroomUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(AddClassroomUseCase request, CancellationToken cancellationToken)
        {
            var classroom = new Classroom
            {
                Id = Guid.NewGuid(),
                OfficeId = request.OfficeId,
                Capacity = request.Capacity
            };

            _db.Classrooms.Add(classroom);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
