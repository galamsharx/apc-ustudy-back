﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class GetExamsUseCase : IRequest<List<Exam>>
    {

    }

    public class GetExamsUseCaseHandler : IRequestHandler<GetExamsUseCase, List<Exam>>
    {
        private readonly PlannerContext _db;

        public GetExamsUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        public Task<List<Exam>> Handle(GetExamsUseCase request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_db.Exams.ToList());
        }
    }
}
