﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Auth.Exceptions;
using UPlanner.Application.Auth.Services;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Infrastructure.Auth.Services
{
    public class DefaultSecurityContext : ISecurityContext
    {
        private readonly PlannerContext _db;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public DefaultSecurityContext(
            PlannerContext db,
            IHttpContextAccessor httpContextAccessor)
        {
            _db = db;
            _httpContextAccessor = httpContextAccessor;
        }

        public bool UserExists
        {
            get
            {
                return _httpContextAccessor.HttpContext?.User?.Identity is not null;
            }
        }

        public Guid UserId
        {
            get
            {
                if (!UserExists)
                {
                    throw new NotAuthenticatedException();
                }
                try
                {
                    var subject = _httpContextAccessor.HttpContext.User.FindFirst("sub").Value;

                    return Guid.Parse(subject);
                }
                catch (Exception exception) when (exception is FormatException or ArgumentNullException)
                {
                    throw new NotAuthenticatedException();
                }
            }
        }

        public async Task<User> GetUserAsync(CancellationToken cancellationToken = default)
        {
            return await _db.Users.FindAsync(keyValues: new object[] { UserId }, cancellationToken);
        }
    }
}
