﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;
using UPlanner.Application.Auth.Services;
using UPlanner.Domain.Models;

namespace UPlanner.Infrastructure.Auth.Services
{
    public class DefaultTokenFactory : ITokenFactory
    {
        private readonly DefaultTokenFactoryOptions _options;
        private readonly JsonWebTokenHandler _tokenHandler;

        public DefaultTokenFactory(IOptions<DefaultTokenFactoryOptions> options)
        {
            _options = options.Value;
            _tokenHandler = new();
        }

        public string CreateToken(User user)
        {
            var claims = GetClaims(user);

            return CreateToken(claims);
        }

        private string CreateToken(Dictionary<string, object> claims)
        {
            var signingCredentials = new SigningCredentials(
                _options.CreateKey(),
                SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Claims = claims,
                Issuer = _options.Issuer,
                Audience = _options.Audience,
                NotBefore = DateTime.UtcNow,
                Expires = DateTime.UtcNow.Add(_options.TokenLifetime),
                SigningCredentials = signingCredentials,
            };

            return _tokenHandler.CreateToken(tokenDescriptor);
        }

        private static Dictionary<string, object> GetClaims(User user)
        {
            var claims = new Dictionary<string, object>
            {
                ["sub"] = user.Id.ToString(),
                ["role"] = user.Role.ToString()
            };

            return claims;
        }
    }

    public class DefaultTokenFactoryOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public TimeSpan TokenLifetime { get; set; }
        public string SecretKey { get; set; }

        public SymmetricSecurityKey CreateKey()
        {
            return new(Encoding.ASCII.GetBytes(SecretKey));
        }
    }
}
