﻿using System;
using System.Text;
using UPlanner.Application.Auth.Services;

namespace UPlanner.Infrastructure.Auth.Services
{
    public class DefaultPasswordGenerator : IPasswordGenerator
    {
        public string GeneratePassword()
        {
            const string chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

            StringBuilder sb = new StringBuilder();
            Random rnd = new Random();

            for (int i = 0; i < 10; i++)
            {
                int index = rnd.Next(chars.Length);
                sb.Append(chars[index]);
            }

            return sb.ToString();
        }
    }
}
